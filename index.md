<h1 style="text-decoration: underline overline;"> Welcome to web-mark beta production page</h1>

## What is web-mark
It is a project to brake restrictions of accessibility when surf over different devices.
 It will do the best for everyone, everything, everywhere, without worrying about limitations.
 Because it will taken as a choice by the project.

## Aim
- Speed
- Compatibility
- Non-stop Resources collections
- Remove barriers without losing capabilities to changing time and space
<hr/>

## Activate web-mark
If you would like to add WebMark framework styling in your website projects just copy and paste code given below inside head tag. Then read examples given in accessories page. 

<div onclick="copyMyinp();"  style="user-select:all;" class="language-markdown highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">"stylesheet"</span> <span class="na">type=</span><span class="s">"text/css"</span> <span class="na">href=</span><span class="s">"https://amalbenny.mooo.com/wm/beta.css"</span><span class="nt">&gt;</span>
</code></pre></div></div>
<script type="text/javascript">

function copyMyinp(){	
	document.execCommand("copy");
}

</script>

## <a href="accessoires/">Accessories</a>
Accessoires helps to learn and explore with Web-Mark. They are training materials for newcomers



## <a href="https://github.com/amalbenny/web-mark/wiki">Web-mark wiki</a>
Here you can discuss anything about this project.

## <a href="https://github.com/amalbenny/web-mark/blob/main/stylesheet.css">Stylesheet raw</a>
Raw file of styles.css
